#include <Servo.h>

/* Configuration Stuff */
#define BEGIN_POSITION  59.0
#define END_POSITION    99.0
#define ACTUATOR_DELTA  0.5  // 1.0mm steps
#define DELAY_MS        50   // 50ms per step


/*---------------------- Servo commands ------------------------*/
#define PIN_ACT         9

Servo linearAct;

void SetStrokePercent(float percentage) {
  if ( percentage >= 1.0 && percentage <= 99.0 ) {
    int usec = 1000 + percentage * ( 2000 - 1000 ) / 100.0 ;
    linearAct.writeMicroseconds( usec );
  }
}

void SetupLinearActuator() {
  // Attach the Correct Pin to the Linear Actuator
  linearAct.attach(PIN_ACT);
  // Set the default position to be extended
  SetStrokePercent(BEGIN_POSITION);
  delay(1000);
}

/*---------------------------------------------------------------*/

/*---------------------- Mathematical stuff ---------------------*/
#define MYPI 3.14159265358979323846

static const float lut_mean = (BEGIN_POSITION + END_POSITION)/2.0;
static const float lut_range = abs(BEGIN_POSITION - END_POSITION);
static const int lut_size = (int)(lut_range/ACTUATOR_DELTA);


/*------------------------ Main Exec Loop -----------------------*/

void setup() {
  Serial.begin(9600);
//  GenerateLUT();
  SetupLinearActuator();
}

void loop() {
  static int t = 0;
  static int m = 1;
  float cmd;
  while(1) {
    cmd = lut_mean + (lut_range/2)*cos(t*2.0*MYPI/(lut_size));
//    cmd = BEGIN_POSITION - 
    Serial.println(cmd);
    SetStrokePercent(cmd);
    t = (t+m)%lut_size;  
    delay(DELAY_MS);
//    delayMicroseconds(DELAY_US);
  }
}

/*---------------------------------------------------------------*/
